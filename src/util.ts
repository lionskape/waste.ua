type Directions = "parentNode";

export function siblings(element: Element) {
    const result: Element[] = [];
    let node = element.parentNode!.firstChild;

    while (node) {
        if (node !== element && node.nodeType === Node.ELEMENT_NODE) {
            result.push(node as Element);
        }
        node = node.nextSibling;
    }

    return result;
}

export function dir<T extends HTMLElement>( elem: T, direction: Directions, until?: Node ): Array<T> {
    const matched = [],
        truncate = until !== undefined;

    let nextNode: Node & ParentNode | null = elem;

    while ( ( nextNode = nextNode[ direction ] ) && nextNode.nodeType !== Node.DOCUMENT_NODE ) {
        console.log(nextNode);
        if ( nextNode.nodeType === Node.ELEMENT_NODE ) {
            if ( truncate && nextNode === until ) {
                break;
            }
            matched.push( nextNode as T );
        }
    }
    return matched;
}

export function parents(child: HTMLElement, selector?: string, until?: Node ) {
    const matched = dir(child, 'parentNode', until);

    if (selector === undefined) {
        return matched;
    }

    return matched.filter((element) => element.matches(selector));
}

export const isOperaMini: boolean = (navigator.userAgent.indexOf('Opera Mini') > -1);
