if (typeof window !== 'undefined' &&  window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = function (callback, thisArg) {
        thisArg = thisArg || window;
        for (let i = 0; i < this.length; i++) {
            callback.call(thisArg, this[i], i, this);
        }
    };
}

if (!Element.prototype.matches) {
    Element.prototype.matches =
        // @ts-ignore
        Element.prototype.matchesSelector ||
        // @ts-ignore
        Element.prototype.mozMatchesSelector ||
        // @ts-ignore
        Element.prototype.msMatchesSelector ||
        // @ts-ignore
        Element.prototype.oMatchesSelector ||
        Element.prototype.webkitMatchesSelector ||
        function(s) {
            // @ts-ignore
            let matches = (this.document || this.ownerDocument).querySelectorAll(s),
                i = matches.length;
            // @ts-ignore
            while (--i >= 0 && matches.item(i) !== this) {}
            return i > -1;
        };
}
