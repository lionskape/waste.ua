import "./polyfill.ts"
import "./components/aside-menu/aside-menu.ts"
import { isOperaMini } from "~util";

document.addEventListener('DOMContentLoaded', () => {
    if (isOperaMini) {
        document.querySelector('body')!.classList.add('opera-mini');
    }
});
