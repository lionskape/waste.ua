import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';
import { parents, siblings } from "~util";

document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll<HTMLElement>('.aside-menu__item').forEach((element) => {
        element.addEventListener('click', (event) => {
            if (elementContainsLink(element)) {
                return;
            }

            event.preventDefault();
            event.stopPropagation();

            element.classList.remove('mobile-hidden');

            const container = document.querySelector<HTMLDivElement>('.aside-menu')!;
            openMenuItem(container, element);

            if (!container.classList.contains('menu-mobile-open')) {
                container.classList.add('menu-mobile-open');
                lockMobileScroll(container);
            }
        });
    });

    const closeIcon = document.querySelector<HTMLButtonElement>('.aside-menu__button-close')!;
    closeIcon.addEventListener('click', (event) => {
        event.preventDefault();
        event.stopPropagation();

        closeMenu();
    });

    document.addEventListener('click', () => {
        closeMenu();
    });
});

function elementContainsLink(element: Element) {
    const link = element.getAttribute('href');
    return !(!link || link === '#');
}

function closeMenu() {
    const container = document.querySelector<HTMLDivElement>('.aside-menu')!;
    container.classList.remove('menu-mobile-open');

    container.querySelectorAll('.mobile-hidden').forEach((element) => {
        element.classList.remove('mobile-hidden');
    });

    container.querySelectorAll('.aside-menu__item--click').forEach((element) => {
        element.classList.remove('aside-menu__item--click');
    });

    unlockMobileScroll(container);
}

function closeMenuItem(menuItem: HTMLElement) {
    siblings(menuItem).forEach((element) => {
        element.classList.remove('mobile-hidden');
        element.classList.remove('aside-menu__item--click');
    });
    menuItem.classList.remove('aside-menu__item--click');
}

function openMenuItem(container: HTMLElement, menuItem: HTMLElement) {
    container.querySelectorAll('.aside-menu__item').forEach((element) => {
        element.classList.remove('mobile-hidden');
    });

    container.querySelectorAll('.aside-menu__item--click').forEach((element) => {
        element.classList.remove('aside-menu__item--click');
    });

    selectMenuItem(menuItem);
    parents(menuItem, '.aside-menu__item', container).forEach((parentMenuItem) => {
        selectMenuItem(parentMenuItem);
    });
}

function selectMenuItem(menuItem: HTMLElement) {
    menuItem.classList.add('aside-menu__item--click');
    siblings(menuItem).forEach((node) => {
        node.classList.add('mobile-hidden');
    });
}

function lockMobileScroll(element: Element) {
    if (window.matchMedia("max-width: 950px")) {
        disableBodyScroll(element);
    }
}

function unlockMobileScroll(element: Element) {
    enableBodyScroll(element);
}
